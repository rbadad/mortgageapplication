package com.mortgage.loan.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;


import com.mortgage.loan.demo.entity.Customer;
import com.mortgage.loan.demo.entity.CustomerSavingAccount;

public interface CustomerSavingAccountRepository extends JpaRepository<CustomerSavingAccount, Integer> {

	public CustomerSavingAccount findByCustomer(Customer customer);
}
