package com.mortgage.loan.demo.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mortgage.loan.demo.entity.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer>{

	public Customer findByCustomerIdAndPassword(String customerId, String password);
	
	public Optional<Customer> findByEmail(String email);

	Customer findByCustomerId(String customerId);

}
