package com.mortgage.loan.demo.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "MORTGAGE_ACCOUNT")
public class MortgageAccount implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Integer mortgageAccId;
	@OneToOne
	@JoinColumn(name = "custId")
	private Customer customer;
	private String mortgageAccNumber;
	private LocalDateTime createdDate;
	private double accountBalance;

	public MortgageAccount() {
		super();
	}

	public Integer getMortgageAccId() {
		return mortgageAccId;
	}

	public void setMortgageAccId(Integer mortgageAccId) {
		this.mortgageAccId = mortgageAccId;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public String getMortgageAccNumber() {
		return mortgageAccNumber;
	}

	public void setMortgageAccNumber(String mortgageAccNumber) {
		this.mortgageAccNumber = mortgageAccNumber;
	}

	public LocalDateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}

	public double getAccountBalance() {
		return accountBalance;
	}

	public void setAccountBalance(double accountBalance) {
		this.accountBalance = accountBalance;
	}

	@Override
	public String toString() {
		return "MortgageAccount [mortgageAccId=" + mortgageAccId + ", customer=" + customer + ", mortgageAccNumber="
				+ mortgageAccNumber + ", createdDate=" + createdDate + ", accountBalance=" + accountBalance + "]";
	}

}