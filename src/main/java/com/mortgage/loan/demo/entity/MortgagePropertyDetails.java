package com.mortgage.loan.demo.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "MORTGAGE_PROPERTY_DETAILS")
public class MortgagePropertyDetails implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Integer mortgageProId;
	private double propertyCost;
	private double deposit;
	private String propertyType;
	private String propertyOwner;
	private String comment;
	@OneToOne
	@JoinColumn(name = "mortgageAccId")
	private MortgageAccount mortgageAccount;
	private LocalDateTime mortgagePropertyDate;

	public MortgagePropertyDetails() {
		super();
	}

	public Integer getMortgageProId() {
		return mortgageProId;
	}

	public void setMortgageProId(Integer mortgageProId) {
		this.mortgageProId = mortgageProId;
	}

	public double getPropertyCost() {
		return propertyCost;
	}

	public void setPropertyCost(double propertyCost) {
		this.propertyCost = propertyCost;
	}

	public double getDeposit() {
		return deposit;
	}

	public void setDeposit(double deposit) {
		this.deposit = deposit;
	}

	public String getPropertyType() {
		return propertyType;
	}

	public void setPropertyType(String propertyType) {
		this.propertyType = propertyType;
	}

	public String getPropertyOwner() {
		return propertyOwner;
	}

	public void setPropertyOwner(String propertyOwner) {
		this.propertyOwner = propertyOwner;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public MortgageAccount getMortgageAccount() {
		return mortgageAccount;
	}

	public void setMortgageAccount(MortgageAccount mortgageAccount) {
		this.mortgageAccount = mortgageAccount;
	}

	public LocalDateTime getMortgagePropertyDate() {
		return mortgagePropertyDate;
	}

	public void setMortgagePropertyDate(LocalDateTime mortgagePropertyDate) {
		this.mortgagePropertyDate = mortgagePropertyDate;
	}

	@Override
	public String toString() {
		return "MortgagePropertyDetails [mortgageProId=" + mortgageProId + ", propertyCost=" + propertyCost
				+ ", deposit=" + deposit + ", propertyType=" + propertyType + ", propertyOwner=" + propertyOwner
				+ ", comment=" + comment + ", mortgageAccount=" + mortgageAccount + ", mortgagePropertyDate="
				+ mortgagePropertyDate + "]";
	}
}
