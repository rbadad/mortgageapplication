package com.mortgage.loan.demo.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "CUSTOMER_SAVING_ACCOUNT")
public class CustomerSavingAccount implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Integer customerSavingAccId;
	private String customerAccountNumber;
	private String accountType;
	private double balance;
	private LocalDateTime createdDate;
	@OneToOne
	@JoinColumn(name = "custId")
	private Customer customer;

	public CustomerSavingAccount() {
		super();
	}
	public CustomerSavingAccount(Integer customerSavingAccId, String customerAccountNumber, String accountType,
			double balance, LocalDateTime createdDate, Customer customer) {
		super();
		this.customerSavingAccId = customerSavingAccId;
		this.customerAccountNumber = customerAccountNumber;
		this.accountType = accountType;
		this.balance = balance;
		this.createdDate = createdDate;
		this.customer = customer;
	}

	public Integer getCustomerSavingAccId() {
		return customerSavingAccId;
	}

	public void setCustomerSavingAccId(Integer customerSavingAccId) {
		this.customerSavingAccId = customerSavingAccId;
	}
	public String getCustomerAccountNumber() {
		return customerAccountNumber;
	}
	public void setCustomerAccountNumber(String customerAccountNumber) {
		this.customerAccountNumber = customerAccountNumber;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public LocalDateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	@Override
	public String toString() {
		return "CustomerSavingAccount [customerSavingAccId=" + customerSavingAccId + ", customerAccountNumber="
				+ customerAccountNumber + ", accountType=" + accountType + ", balance=" + balance + ", createdDate="
				+ createdDate + ", customer=" + customer + "]";
	}
}
