package com.mortgage.loan.demo.exception;

public class AgeValidException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AgeValidException() {
		super();
	}

	public AgeValidException(String message) {
		super(message);
	}

	
}
