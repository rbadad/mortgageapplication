package com.mortgage.loan.demo.exception;

public class CustomerExistException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CustomerExistException() {
		super();
	}

	public CustomerExistException(String message) {
		super(message);
	}

}
