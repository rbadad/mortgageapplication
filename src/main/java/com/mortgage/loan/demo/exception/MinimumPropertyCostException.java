package com.mortgage.loan.demo.exception;

public class MinimumPropertyCostException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MinimumPropertyCostException() {
		super();
	}

	public MinimumPropertyCostException(String message) {
		super(message);
	}

	
}
