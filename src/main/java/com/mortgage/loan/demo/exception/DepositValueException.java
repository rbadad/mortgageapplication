package com.mortgage.loan.demo.exception;

public class DepositValueException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DepositValueException() {
		super();
	}

	public DepositValueException(String message) {
		super(message);
	}

	
}
