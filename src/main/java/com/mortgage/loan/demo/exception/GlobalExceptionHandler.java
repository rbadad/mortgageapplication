package com.mortgage.loan.demo.exception;

import java.util.stream.Collectors;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.mortgage.loan.demo.constants.AppConstants;

@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler{

	@ExceptionHandler(value = AgeValidException.class)
	public ResponseEntity<ErrorMsgResponse> hanldeAgeValidException() {
		ErrorMsgResponse response = new ErrorMsgResponse();
		response.setMessage(AppConstants.AGE_INVALID);
		response.setStatus(AppConstants.AGE_INVALID_STATUS_CODE);
		return new ResponseEntity<>(response,HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(value = CustomerExistException.class)
	public ResponseEntity<ErrorMsgResponse> hanldeCustomerExistException() {
		ErrorMsgResponse response = new ErrorMsgResponse();
		response.setMessage(AppConstants.CUSTOMER_EXISTS);
		response.setStatus(AppConstants.CUSTOMER_EXISTS_STATUS_CODE);
		return new ResponseEntity<>(response,HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(value = MinimumPropertyCostException.class)
	public ResponseEntity<ErrorMsgResponse> hanldeMinimumPropertyCostException() {
		ErrorMsgResponse response = new ErrorMsgResponse();
		response.setMessage(AppConstants.MINIMUNM_PROPERTY_COST);
		response.setStatus(AppConstants.MINIMUNM_PROPERTY_COST_STATUS_CODE);
		return new ResponseEntity<>(response,HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(value = DepositValueException.class)
	public ResponseEntity<ErrorMsgResponse> hanldeDepositValueException() {
		ErrorMsgResponse response = new ErrorMsgResponse();
		response.setMessage(AppConstants.DEPOSIT_VALUE_NEGATIVE);
		response.setStatus(AppConstants.DEPOSIT_VALUE_COST_STATUS_CODE);
		return new ResponseEntity<>(response,HttpStatus.NOT_FOUND);
	}
	
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		ErrorMsgResponse response = new ErrorMsgResponse();
		response.setStatus(005);
		String allFieldErrors = ex.getBindingResult().getFieldErrors().stream()
				.map(e -> e.getDefaultMessage()).collect(Collectors.joining(", "));
		response.setMessage(allFieldErrors);
		return new ResponseEntity<>(response,HttpStatus.BAD_REQUEST);
	}
}
