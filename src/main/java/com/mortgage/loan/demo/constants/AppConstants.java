package com.mortgage.loan.demo.constants;

import java.time.LocalDateTime;

public class AppConstants {

	public static final String LOGIN_SUCCESS ="Login successful";
	public static final String LOGIN_SUCCESS_STATUS_CODE ="5000";
	public static final String LOGIN_FAILURE ="login failed, please enter valid credentials";
	public static final String LOGIN_FAILURE_STATUS_CODE ="5001";

	
	public static final int AGE_INVALID_STATUS_CODE =6000;
	public static final String AGE_INVALID ="Sorry we are unable to grant you the mortgage at this moment";
	
	public static final int CUSTOMER_EXISTS_STATUS_CODE =6001;
	public static final String CUSTOMER_EXISTS ="Customer exists already with the given email id!";

	public static final int MINIMUNM_PROPERTY_COST_STATUS_CODE =6002;
	public static final String MINIMUNM_PROPERTY_COST ="Minimum property cost should be 100000";

	public static final int DEPOSIT_VALUE_COST_STATUS_CODE =6003;
	public static final String DEPOSIT_VALUE_NEGATIVE ="Deposit value should not be negative";
	
	public static final String EMPLOYMENT_TYPE = "Employed";
	public static final String SAVINGS_ACCOUNT_SUFFIX = "KJPPR00000000";
	public static final String MORTGAGE_ACCOUNT_SUFFIX = "MORTKJPPR0000";
	
	public static final String SAVINGS_ACCOUNT ="Savings Account";

	public static final String CUST = "CUST";


	public static final String CUSTOMER_NOT_FOUND = "Customer Not Found";
	
	private AppConstants() {
		super();
	}
	
	public static final String ONLINE_MODE ="Online Mode";
	public static final String PAID_MONTHLY_EMI ="Paid "+LocalDateTime.now().getMonth()+" month deposit";
}
