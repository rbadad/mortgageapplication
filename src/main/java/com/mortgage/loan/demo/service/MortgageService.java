package com.mortgage.loan.demo.service;

import com.mortgage.loan.demo.dto.MortgageDto;
import com.mortgage.loan.demo.dto.MortgageResponseDto;
import com.mortgage.loan.demo.exception.AgeValidException;
import com.mortgage.loan.demo.exception.CustomerExistException;
import com.mortgage.loan.demo.exception.DepositValueException;
import com.mortgage.loan.demo.exception.MinimumPropertyCostException;

public interface MortgageService {

	/**
	 * 
	 * @param mortgageDto
	 * @return MortgageResponseDto
	 * @throws AgeValidException
	 * @throws CustomerExistException
	 * @throws MinimumPropertyCostException
	 * @throws DepositValueException
	 */
	public MortgageResponseDto mortgageRegister(MortgageDto mortgageDto) throws AgeValidException, CustomerExistException,
	MinimumPropertyCostException, DepositValueException;
}
