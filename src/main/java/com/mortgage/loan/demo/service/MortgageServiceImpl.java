package com.mortgage.loan.demo.service;

import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mortgage.loan.demo.constants.AppConstants;
import com.mortgage.loan.demo.dto.MortgageDto;
import com.mortgage.loan.demo.dto.MortgageResponseDto;
import com.mortgage.loan.demo.entity.Customer;
import com.mortgage.loan.demo.entity.CustomerSavingAccount;
import com.mortgage.loan.demo.entity.MortgageAccount;
import com.mortgage.loan.demo.entity.MortgagePropertyDetails;
import com.mortgage.loan.demo.entity.TransactionDetails;
import com.mortgage.loan.demo.exception.AgeValidException;
import com.mortgage.loan.demo.exception.CustomerExistException;
import com.mortgage.loan.demo.exception.DepositValueException;
import com.mortgage.loan.demo.exception.MinimumPropertyCostException;
import com.mortgage.loan.demo.repository.CustomerRepository;
import com.mortgage.loan.demo.repository.CustomerSavingAccountRepository;
import com.mortgage.loan.demo.repository.MortgageAccountRepository;
import com.mortgage.loan.demo.repository.MortgagePropertyDetailsRepository;
import com.mortgage.loan.demo.repository.TransactionDetailsRepository;

/**
 * 
 * @author janbee
 *
 */
@Service
public class MortgageServiceImpl implements MortgageService{

	private static final Logger LOGGER = LoggerFactory.getLogger(MortgageServiceImpl.class);
	
	@Autowired
	CustomerRepository customerRepository;
	@Autowired
	MortgageAccountRepository mortgageAccountRepository;
	@Autowired
	MortgagePropertyDetailsRepository mortgagePropertyDetailsRepository;
	@Autowired
	CustomerSavingAccountRepository savingAccountRepository;
	@Autowired
	TransactionDetailsRepository transactionDetailsRepository;

	/**
	 * 
	 */
	@Override
	public MortgageResponseDto mortgageRegister(MortgageDto mortgageDto) throws AgeValidException, CustomerExistException, MinimumPropertyCostException, DepositValueException {
		LOGGER.info("MortgageServiceImpl :: mortgageRegister :: start");
		Customer customer;
		CustomerSavingAccount savingAccount;
		MortgageAccount mortgageAccount;
		MortgagePropertyDetails mortgagePropertyDetails;

		LocalDateTime dob = mortgageDto.getDob();
		LocalDateTime todayDate = LocalDateTime.now();
		long years = ChronoUnit.YEARS.between(dob,todayDate);


		String email = mortgageDto.getEmail();
		Optional<Customer> cust = customerRepository.findByEmail(email);
		if(cust.isPresent()) {
			throw new CustomerExistException();
		}
		if(years<18) {
			throw new AgeValidException();
		}
		double propertyCost = mortgageDto.getPropertyCost();
		if(propertyCost<100000.0) {
			throw new MinimumPropertyCostException();
		}
		double deposit = mortgageDto.getDeposit();
		if(deposit<0) {
			throw new DepositValueException();
		}

		customer = new Customer();
		String employmentType = mortgageDto.getCustomerEmployeeType();
		if(employmentType.equals(AppConstants.EMPLOYMENT_TYPE)) {
			customer.setContractType(mortgageDto.getContractType());
			customer.setOccupationDoj(mortgageDto.getOccupationDoj());
		}
		customer.setCustomerEmployeeType(employmentType);
		Random random = new Random();
		customer.setCustomerId(AppConstants.CUST+ random.nextInt(1000));
		customer.setCustomerMiddleName(mortgageDto.getCustomerMiddleName());
		customer.setCustomerOccupation(mortgageDto.getCustomerOccupation());
		customer.setCustomerSurName(mortgageDto.getCustomerSurName());
		customer.setCustomerTitle(mortgageDto.getCustomerTitle());
		customer.setDob(dob);
		customer.setEmail(email);
		customer.setMobileNumber(mortgageDto.getMobileNumber());
		customer.setPassword(encryptPassword(mortgageDto.getPassword()));
		Customer newCustObj = customerRepository.save(customer);

		savingAccount = new CustomerSavingAccount();
		savingAccount.setAccountType(AppConstants.SAVINGS_ACCOUNT);
		savingAccount.setBalance(mortgageDto.getPropertyCost() - mortgageDto.getDeposit());
		savingAccount.setCreatedDate(LocalDateTime.now());
		savingAccount.setCustomer(newCustObj);
		savingAccount.setCustomerAccountNumber(generateSavingAccountNumber());
		CustomerSavingAccount newSavingAccount = savingAccountRepository.save(savingAccount);

		mortgageAccount = new MortgageAccount();
		mortgageAccount.setAccountBalance(-(mortgageDto.getPropertyCost() - mortgageDto.getDeposit()));
		mortgageAccount.setCreatedDate(LocalDateTime.now());
		mortgageAccount.setCustomer(newCustObj);
		mortgageAccount.setMortgageAccNumber(	generateMortgageAccountNumber());
		MortgageAccount newMortgageAccount = mortgageAccountRepository.save(mortgageAccount);

		mortgagePropertyDetails = new MortgagePropertyDetails();
		mortgagePropertyDetails.setDeposit(deposit);
		mortgagePropertyDetails.setMortgageAccount(newMortgageAccount);
		mortgagePropertyDetails.setMortgagePropertyDate(LocalDateTime.now());
		mortgagePropertyDetails.setPropertyCost(mortgageDto.getPropertyCost());
		mortgagePropertyDetails.setPropertyOwner(mortgageDto.getPropertyOwner());
		mortgagePropertyDetails.setPropertyType(mortgageDto.getPropertyType());
		mortgagePropertyDetailsRepository.save(mortgagePropertyDetails);

		TransactionDetails transactionDetails = new TransactionDetails();
		transactionDetails.setCustomer(newCustObj);
		transactionDetails.setTransactionalAccBalance(deposit);
		transactionDetails.setTransactionType(AppConstants.ONLINE_MODE);
		transactionDetails.setTranscationDate(LocalDateTime.now());
		transactionDetails.setTransComment(AppConstants.PAID_MONTHLY_EMI);
		transactionDetailsRepository.save(transactionDetails);
		
		MortgageResponseDto response = new MortgageResponseDto();
		response.setCustomerAccountNumber(newSavingAccount.getCustomerAccountNumber());
		response.setCustomerId(newCustObj.getCustomerId());
		response.setMortgageAccNumber(newMortgageAccount.getMortgageAccNumber());
		response.setPassword(newCustObj.getPassword());
		LOGGER.info("MortgageServiceImpl :: mortgageRegister :: end");
		return response;
	}


	private String encryptPassword(String password) {
		String base64encodedString = null;
		try {
			base64encodedString = Base64.getEncoder().encodeToString(
					password.getBytes("utf-8"));
		} catch (UnsupportedEncodingException e) {
			LOGGER.error("MortgageServiceImpl :: encryptPassword :: exception");
		}
		return base64encodedString;
	}
	static int savingCount =300;
	static int mortCount =500;
	private static String generateSavingAccountNumber() {
		return AppConstants.SAVINGS_ACCOUNT_SUFFIX+ (savingCount++);
	}

	private static String generateMortgageAccountNumber() {
		return AppConstants.MORTGAGE_ACCOUNT_SUFFIX+ (mortCount++);
	}
	
	// updating EMI transaction here
	public void JobUpdateTransations() {
		LOGGER.info("job is running for every 5-sec");
		Double monthlyEmiAmt = 20000.0;
		List<Customer> custList = customerRepository.findAll();
		for (Customer customer : custList) {
			CustomerSavingAccount csa = savingAccountRepository.findByCustomer(customer);
			MortgageAccount mortgageAcc = mortgageAccountRepository.findByCustomer(customer);
			if(csa.getBalance()>0 && mortgageAcc.getAccountBalance()<0) {
				Double accBalance = csa.getBalance();
				Double remainingBalnc = (accBalance-monthlyEmiAmt);
				csa.setBalance(remainingBalnc);
				savingAccountRepository.save(csa);
				Double mortAccBalnc = mortgageAcc.getAccountBalance();
				Double RemMortAccBalce = (-mortAccBalnc+monthlyEmiAmt);
				mortgageAcc.setAccountBalance(RemMortAccBalce);
				mortgageAccountRepository.save(mortgageAcc);
				TransactionDetails transactionDetails = new TransactionDetails();
				transactionDetails.setCustomer(customer);
				transactionDetails.setTransComment("paid EMI "+ monthlyEmiAmt);
				transactionDetails.setTransactionalAccBalance(monthlyEmiAmt);
				transactionDetails.setTranscationDate(LocalDateTime.now());
				transactionDetails.setCustomer(customer);
				transactionDetailsRepository.save(transactionDetails);
			}else {
				LOGGER.info("MONTHLY PAYING EMI COMPLETED!!!!!!!");
			}
		}
				
	}
}
