package com.mortgage.loan.demo.dto;

import java.time.LocalDateTime;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class MortgageDto {

	@NotEmpty(message = "FirstName is required")
	private String customerFirstName;
	private String customerMiddleName;
	@NotEmpty(message = "SurName is required")
	private String customerSurName;
	private LocalDateTime dob;
	@Email(message = "Please enter the valid email")
	private String email;
	private String customerOccupation;
	private LocalDateTime occupationDoj;
	@NotEmpty(message = "Mobile Number is required")
	@Size(min = 0, max = 10)
	private String mobileNumber;
	private String customerEmployeeType;
	@NotEmpty(message = "Password is required")
	private String password;
	private double propertyCost;
	private double deposit;
	private String propertyType;
	private String propertyOwner;
	private String contractType;
	private String customerTitle;
	public String getCustomerFirstName() {
		return customerFirstName;
	}
	public void setCustomerFirstName(String customerFirstName) {
		this.customerFirstName = customerFirstName;
	}
	public String getCustomerMiddleName() {
		return customerMiddleName;
	}
	public void setCustomerMiddleName(String customerMiddleName) {
		this.customerMiddleName = customerMiddleName;
	}
	public String getCustomerSurName() {
		return customerSurName;
	}
	public void setCustomerSurName(String customerSurName) {
		this.customerSurName = customerSurName;
	}
	public LocalDateTime getDob() {
		return dob;
	}
	public void setDob(LocalDateTime dob) {
		this.dob = dob;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCustomerOccupation() {
		return customerOccupation;
	}
	public void setCustomerOccupation(String customerOccupation) {
		this.customerOccupation = customerOccupation;
	}
	public LocalDateTime getOccupationDoj() {
		return occupationDoj;
	}
	public void setOccupationDoj(LocalDateTime occupationDoj) {
		this.occupationDoj = occupationDoj;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getCustomerEmployeeType() {
		return customerEmployeeType;
	}
	public void setCustomerEmployeeType(String customerEmployeeType) {
		this.customerEmployeeType = customerEmployeeType;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public double getPropertyCost() {
		return propertyCost;
	}
	public void setPropertyCost(double propertyCost) {
		this.propertyCost = propertyCost;
	}
	public double getDeposit() {
		return deposit;
	}
	public void setDeposit(double deposit) {
		this.deposit = deposit;
	}
	public String getPropertyType() {
		return propertyType;
	}
	public void setPropertyType(String propertyType) {
		this.propertyType = propertyType;
	}
	public String getPropertyOwner() {
		return propertyOwner;
	}
	public void setPropertyOwner(String propertyOwner) {
		this.propertyOwner = propertyOwner;
	}
	public String getContractType() {
		return contractType;
	}
	public void setContractType(String contractType) {
		this.contractType = contractType;
	}
	public String getCustomerTitle() {
		return customerTitle;
	}
	public void setCustomerTitle(String customerTitle) {
		this.customerTitle = customerTitle;
	}

}
