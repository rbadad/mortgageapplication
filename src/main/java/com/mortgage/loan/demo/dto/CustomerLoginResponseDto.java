package com.mortgage.loan.demo.dto;

public class CustomerLoginResponseDto {

	private String statusCode;
	private String statusMsg;

	public CustomerLoginResponseDto() {
		super();
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusMsg() {
		return statusMsg;
	}

	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}

}
