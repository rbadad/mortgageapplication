package com.mortgage.loan.demo.dto;

public class CustomerLoginReqDto {

	private String customerId;
	private String password;

	public CustomerLoginReqDto() {
		super();
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
