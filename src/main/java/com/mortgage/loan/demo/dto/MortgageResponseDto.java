package com.mortgage.loan.demo.dto;

public class MortgageResponseDto {

	private String customerId;
	private String password;
	private String customerAccountNumber;
	private String mortgageAccNumber;
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getCustomerAccountNumber() {
		return customerAccountNumber;
	}
	public void setCustomerAccountNumber(String customerAccountNumber) {
		this.customerAccountNumber = customerAccountNumber;
	}
	public String getMortgageAccNumber() {
		return mortgageAccNumber;
	}
	public void setMortgageAccNumber(String mortgageAccNumber) {
		this.mortgageAccNumber = mortgageAccNumber;
	}
	
}
