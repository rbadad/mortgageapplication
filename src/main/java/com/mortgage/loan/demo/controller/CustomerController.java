package com.mortgage.loan.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mortgage.loan.demo.dto.CustomerLoginReqDto;
import com.mortgage.loan.demo.dto.CustomerLoginResponseDto;
import com.mortgage.loan.demo.dto.TransactionDetailsResponseDTO;
import com.mortgage.loan.demo.dto.TransactionSummaryResponseDTO;
import com.mortgage.loan.demo.service.CustomerServiceImpl;
import com.mortgage.loan.demo.service.TransactionSummaryServiceImpl;

@RestController
@RequestMapping("/customers")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CustomerController {

	@Autowired
	private CustomerServiceImpl serviceImpl;

	@Autowired
	TransactionSummaryServiceImpl transactionSummaryServiceImpl;

	@PostMapping("/login")
	public ResponseEntity<CustomerLoginResponseDto> customerLogin(@RequestBody CustomerLoginReqDto reqDto) {
		return new ResponseEntity<>(serviceImpl.customerLogin(reqDto), HttpStatus.OK);
	}

	@GetMapping("/{customerId}/account-summary")
	public ResponseEntity<TransactionSummaryResponseDTO> customerTransactionSummary(
			@PathVariable("customerId") String customerId) {
		TransactionSummaryResponseDTO response = transactionSummaryServiceImpl.getTransactionsSummary(customerId);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@GetMapping("/{customerId}/transaction-details")
	public ResponseEntity<List<TransactionDetailsResponseDTO>> customerTransactionDetails(
			@PathVariable("customerId") String customerId) {
		List<TransactionDetailsResponseDTO> response = transactionSummaryServiceImpl.getTransactionDetails(customerId);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
}
