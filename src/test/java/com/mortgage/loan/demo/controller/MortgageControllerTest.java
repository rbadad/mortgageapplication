package com.mortgage.loan.demo.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;

import java.time.LocalDateTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;

import com.mortgage.loan.demo.dto.MortgageDto;
import com.mortgage.loan.demo.dto.MortgageResponseDto;
import com.mortgage.loan.demo.exception.AgeValidException;
import com.mortgage.loan.demo.exception.CustomerExistException;
import com.mortgage.loan.demo.exception.DepositValueException;
import com.mortgage.loan.demo.exception.MinimumPropertyCostException;
import com.mortgage.loan.demo.service.MortgageServiceImpl;

public class MortgageControllerTest {

	@InjectMocks
	MortageController mortgageController;

	@Mock
	MortgageServiceImpl mortgageServiceImpl;


	@BeforeEach
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void mortgageRegisterTest() throws AgeValidException, CustomerExistException, MinimumPropertyCostException, DepositValueException {
		MortgageDto requestDto = new MortgageDto();
		requestDto.setContractType("Permanent");
		requestDto.setCustomerEmployeeType("Employed");
		requestDto.setCustomerFirstName("janbee");
		requestDto.setCustomerMiddleName("");
		requestDto.setCustomerOccupation("Engineer");
		requestDto.setCustomerSurName("Shaik");
		requestDto.setCustomerTitle("Ms.");
		requestDto.setDeposit(20000);
		requestDto.setDob(LocalDateTime.of(1993, 05, 8, 12, 30));
		requestDto.setEmail("janbee@gmail.com");
		requestDto.setMobileNumber("8883483484");
		requestDto.setOccupationDoj(LocalDateTime.of(2015, 05, 8, 12, 30));
		requestDto.setPassword("janbee");
		requestDto.setPropertyCost(100000);
		requestDto.setPropertyOwner("1");
		requestDto.setPropertyType("Buying first home");
		
		MortgageResponseDto responseDto = new MortgageResponseDto();
		responseDto.setCustomerAccountNumber("KJPPR00000000300");
		responseDto.setCustomerId("CUST123");
		responseDto.setMortgageAccNumber("MORTKJPPR0000500");
		responseDto.setPassword("janbee");
		doReturn(responseDto).when(mortgageServiceImpl).mortgageRegister(requestDto);
		ResponseEntity<MortgageResponseDto> mortgageResponse = mortgageController.mortgageRegister(requestDto);

		assertEquals("KJPPR00000000300", mortgageResponse.getBody().getCustomerAccountNumber());
		assertEquals("MORTKJPPR0000500", mortgageResponse.getBody().getMortgageAccNumber());
	}

}
